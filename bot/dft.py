# BOTLIB - Framework to program bots.
#
#

""" default values. """

import lo

#:
default_irc = {
    "channel": "#%s" % (lo.cfg.name or "mybot"),
    "nick": lo.cfg.name or "mybot",
    "ipv6": False,
    "port": 6667,
    "server": "localhost",
    "ssl": False,
    "realname": lo.cfg.name or "mybot",
    "username": lo.cfg.name or "mybot"
}

#:
default_krn = {
    "workdir": "",
    "kernel": False,
    "modules": "",
    "options": "",
    "prompting": True,
    "dosave": False,
    "level": "",
    "logdir": "",
    "shell": False
}

#:
default_rss = {
    "display_list": "title,link",
    "dosave": True,
    "tinyurl": False
}

#:
defaults = lo.Object()
defaults["bot.irc.Cfg"] = lo.Object(default_irc)
defaults["lo.krn.Cfg"] = lo.Object(default_krn)
defaults["bot.rss.Cfg"] = lo.Object(default_rss)
