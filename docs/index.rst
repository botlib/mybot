.. title:: no copyright, no LICENSE, placed in the public domain

M Y B O T
#########

Welcome to  M Y B O T, a pure python3 IRC bot, see https://pypi.org/project/mybot/, placed in the Public Domain. ;]

M Y B O T  can fetch RSS feeds, lets you program your own commands, can work as a UDP to IRC
relay, has user management to limit access to prefered users and can run as a service to let
it restart after reboots.
M Y B O T  is the result of 20 years of programming bots, was there in 2000, is here in
2020, has no copyright, no LICENSE and is placed in the Public Domain. This
makes  M Y B O T  truely free (pastable) code you can use how you see fit, i
hope you enjoy using and programming  M Y B O T  till the point you start
programming your own bots yourself. Have fun coding ;]

you can download with pip3, see https://pypi.org/project/mybot/#files if you need a
tarball.

::

 > sudo pip3 install mybot 

if you want to run the bot 24/7 you can install  M Y B O T  as a service for
the systemd daemon. You can do this by running the mycfg program which let's you 
enter <server> <channel> <nick> <modules> <owner> on the command line:

::

 > sudo mycfg localhost \#mybot mybot bot.irc,bot.rss ~bart@127.0.0.1

mycfg installs a service file in /etc/systemd/system, installs data in /var/lib/mybot and runs myhup to restart the service with the new configuration.
logs are in /var/log/mybot/mybot.log. If you don't want mybot to start at boot, remove the mybot.service file:

::

 > sudo rm /etc/systemd/system/mybot.service 

U S A G E
=========

the basic program is called mybot, you can run it by tying mybot on the
prompt, it will return with its own prompt:

::

 > mybot
 > cmds
 cfg|cmds|fleet|mods|ps|up|v

if you provide mybot with an argument it will run the bot command directly:

::

 > mybot cmds
 cfg|cmds|ed|fleet|mods|ps|up|v

with the -m option you can provide a comma seperated list of modules to load:

::

 > mybot -m bot.rss rss
 https://www.telegraaf.nl/rss


use the --help command to display a help page: 

::

 > mybot --help

 usage: .

  > mybot                - starts a shell
  > mybot <cmd>          - executes a command
  > mybot cmds		 - shows list of commands
  > mybot -m <mod1,mod2> - load modules
  > mybot mods		 - shows loadable modules
  > mybot -w <dir>       - use directory as workdir, default is ~/.mybot
  > mybot cfg		 - show configuration
  > mybot -d             - run as daemon
  > mybot -r		 - root mode, use /var/lib/mybot
  > mybot -o <op1,op2>   - set options
  > mybot -l <level>     - set loglevel

 example:

  > mybot -m bot.irc -s localhost -c \#dunkbots -n mybot --owner root@shell

I R C
=====

IRC (bot.irc) need the -s <server> | -c <channel> | -n <nick> | --owner <userhost> options:

::

 > mybot -m bot.irc -s localhost -c \#dunkbots -n mybot --owner ~bart@192.168.2.1 

for a list of modules to use see the mods command.

::

 > mybot -m bot.shw mods
 bot.ed|bot.irc|bot.dft|bot.krn|bot.usr|bot.shw|bot.udp|bot.ent|bot.rss|bot.flt|bot.fnd

U S E R S
=========

The bot only allows communication to registered users. You can add the
userhost of the owner with the --owner option:

::

 > mybot --owner root@shell
 > ok

The owner of the bot is also the only one who can add other users to the
bot:

::

 > mybot meet ~dunker@jsonbot/daddy
 > ok

R S S
=====

the rss plugin uses the feedparser package, you need to install that yourself:

::

 > pip3 install feedparser

starts the rss fetcher with -m bot.rss.

to add an url use the rss command with an url:

::

 > mybot rss https://news.ycombinator.com/rss
 ok 1

run the rss command to see what urls are registered:

::

 > mybot rss
 0 https://news.ycombinator.com/rss

the fetch command can be used to poll the added feeds:

::

 > mybot fetch
 fetched 0

U D P
=====

using udp to relay text into a channel, use the myudp program to send text via the bot 
to the channel on the irc server:

::

 > tail -f ~/.mybot/logs/mybot.log | myudp 

to send a message to the IRC channel, send a udp packet to the bot:

::

 import socket

 def toudp(host=localhost, port=5500, txt=""):
     sock = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
     sock.sendto(bytes(txt.strip(), "utf-8"), host, port)

P R O G R A M M I N G
=====================

basic code is a function that gets an event as a argument:

::

 def command(event):
     << your code here >>

to give feedback to the user use the event.reply(txt) method:

::

 def command(event):
     event.reply("yooo %s" % event.origin)


You can add you own modules to the mybot package and if you want you can
create your own package with commands in the mybot namespace.


C O D E
=======

if you want to develop on the bot clone the source at bitbucket.org:

::

 > git clone https://bitbucket.org/botlib/mybot

.. _source:

M Y B O T  contains the following modules, borowed from BOTLIB:

.. autosummary::
    :toctree: 
    :template: module.rst

    bot			- botlib
    bot.dft             - default
    bot.ent		- log,todo
    bot.irc             - irc bot
    bot.rss             - rss to channel
    bot.udp             - udp to channel

BOTLIB uses the LIBOBJ library which also gets included in the package:

.. autosummary::
    :toctree: 
    :template: module.rst

    lo			- libobj
    lo.clk              - clock
    lo.csl              - console 
    lo.flt		- fleet
    lo.gnr		- generic
    lo.hdl              - handler
    lo.krn             	- core handler
    lo.shl              - shell
    lo.shw             	- show runtime
    lo.thr              - threads
    lo.tms              - times
    lo.trc              - trace
    lo.typ              - types
    lo.usr             	- users

C O N T A C T
=============

you can contact me on IRC/freenode/#dunkbots or email me at bthate@dds.nl

| Bart Thate (bthate@dds.nl, thatebart@gmail.com)
| botfather on #dunkbots irc.freenode.net
