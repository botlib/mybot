import bot

def cmds(event):
    k = bot.get_kernel()
    b = k.fleet.by_orig(event.orig)
    if b and b.cmds:
        event.reply("|".join(sorted(b.cmds)))
